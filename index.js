const  express = require('express');
//import sin etma script 6
const {BASE_URL, BASE_URL_V2, personasPATH} = require('./routes');
const PORT = 3000;
const personas = [
    {
        _id: 1,
        nombre: 'Jhonatan Iberico',
        edad: 23,
        job: 'FullStack Developer'
    },
    {
        _id: 2,
        nombre: 'Juan Pérez',
        edad: 24,
        job: 'Administrador de sistemas'
    },
    {
        _id: 3,
        nombre: 'Pepe Castillo',
        edad: 25,
        job: 'Analista de datos'
    },
];

const personas2 = [
    {
        _id: 1,
        nombre: 'Jhonatan Iberico',
        edad: 23,
        job: 'FullStack Developer',
        isSingle: false
    },
    {
        _id: 2,
        nombre: 'Juan Pérez',
        edad: 24,
        job: 'Administrador de sistemas',
        isSingle: true
    },
    {
        _id: 3,
        nombre: 'Pepe Castillo',
        edad: 25,
        job: 'Analista de datos',
        isSingle: false
    },
];
const app = express();
app.use(express.json());

app.get('/', (req, res) => {
    return res.sendFile(__dirname.concat('/index.html'), err =>{
        if(err){
            console.error(err);
        }
    })
});
app.get('/testjson', (req, res) => {
    const body = {
        saludo: 'Hola estoy en mi primera ruta con NodeJs y Express Js'
    }
    return res.send(body.saludo);
});

app.get(`${BASE_URL}${personasPATH}`, (_, res) => {
    return res.send(personas);
})

app.post(`${BASE_URL}${personasPATH}`, (request, response) => {
    try {
        const { nombre, edad, job } = request.body;
        if(nombre && edad && job){
            //destructuring

            const nuevaPersona = {
                _id: personas.length+1,
                nombre,
                edad,
                job
            };
            personas.push(nuevaPersona);

            return response.status(201).send({message: 'Persona agregada correctamente '})
        }

        return response.status(500).send({
            message: 'Datos incorrectos',
            nombre,
            edad,
            job
        });
    }catch (e) {
        console.log(e);
        response.status(500).send(
            {error: JSON.stringify(e)}
        );
        return;
    }
});

app.patch(`${BASE_URL}${personasPATH}`, (request, response) => {
    try {
        const persona = request.body;
		if(persona._id ){
			
			const personaIndex = personas.findIndex(value => value._id === persona._id );
            //index
			if(personaIndex > -1 ){
				// CASO DE PROPIEDAD ESPECIFICA
				// const persona = request.body;
				// personas[personaIndex] = { ...personas[personaIndex], nombre }

				personas[personaIndex] = {...persona, _id: personas[personaIndex]._id };
				return response.status(201).send({message: 'Persona actualizada correctamente '});
			}

			return response.status(400).send({message: 'No se han encontrado coincidencias'});
			
		}
		return response.status(403).send({message: 'Faltan campos'});
    }catch (e) {
        console.log(e);
        return response.status(500).send({message: 'Ha ocurrido un error en el servidor'});
    }
});

app.get(`${BASE_URL_V2}${personasPATH}`, (_, res) => {
    res.send(personas2);
});

app.delete(`${BASE_URL}${personasPATH}`, (request, response) => {
    try{
        const persona = request.body;
		if(persona._id ){
            const personaIndex = personas.findIndex(value => value._id === persona._id );
            console.log("arr_obj_antes", personas);
            personas.splice(personaIndex, 1)
            console.log("arr_obj_desp", personas);
            return response.status(201).send({message: 'Persona eliminada correctamente '});
        }
        return response.status(403).send({message: 'Faltan campos'});
    }catch(e) {
        return response.status(500).send({message: 'Ha ocurrido un error en el servidor'});
    }
});

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
